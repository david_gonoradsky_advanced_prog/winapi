#include "Helper.h"
#include "Windows.h"
#include <string>
#include <iostream>

using namespace std;

typedef int (CALLBACK* TheAnswerToLifeTheUniverseAndEverythingType)();

int main()
{
	string input;
	vector<string> words;
	char path[MAX_PATH];
	HANDLE hFile;
	DWORD dwRet;
	LARGE_INTEGER filesize;
	HINSTANCE dllHandle = NULL;
	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	STARTUPINFO startupinfo;
	GetStartupInfo(&startupinfo);
	PROCESS_INFORMATION pro2info;
	TheAnswerToLifeTheUniverseAndEverythingType TheAnswerToLifeTheUniverseAndEverythingPtr = NULL;
	DWORD dwErr = 0;
	BOOL freeResult = false;
	int retVal = 0;
	while (true)
	{
		cout << ">>";
		getline(cin, input);
		Helper::trim(input);
		words = Helper::get_words(input);
		if (!words[0].compare("pwd"))
		{
			dwRet = GetCurrentDirectory(MAX_PATH, path);
			if (dwRet == 0)
			{
				cout << "GetCurrentDirectory failed" << GetLastError() << endl;
				return 1;
			}
			cout << path << endl;
		}
		else if (!words[0].compare("cd") && words.size() > 1)
		{
			strcpy(path, (words[1].c_str()));
			if (!SetCurrentDirectoryA(path))
			{
				cout << "SetCurrentDirectory failed" << GetLastError() << endl;
				return 1;
			}
		}
		else if (!words[0].compare("create") && words.size() > 1)
		{
			strcpy(path, (words[1].c_str()));
			hFile = CreateFile(path, GENERIC_WRITE, 0, NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);
			if (hFile == INVALID_HANDLE_VALUE)
			{
				cout << "Terminal failure: Unable to create file" << path << " for write" << endl;
				return 1;
			}
		}
		else if (!words[0].compare("ls"))
		{
			dwRet = GetCurrentDirectory(MAX_PATH, path);
			if (dwRet)
			{
				input = string(path) + "\\*";
				strcpy(path, (input.c_str()));
				hFind = FindFirstFileA(path, &ffd);
				if (INVALID_HANDLE_VALUE == hFind)
				{
					continue;
				}
				do
				{
					cout << ffd.cFileName << endl;
				} while (FindNextFileA(hFind, &ffd) != 0);
			}
		}
		else if (!words[0].compare("secret"))
		{
			dllHandle = LoadLibrary("C:\\Users\\User\\Documents\\Study\\Magshimim\\2016-2017\\Advanced Programing\\Semester_B\\WinAPI\\secret.dll");
			if (dllHandle)
			{
				TheAnswerToLifeTheUniverseAndEverythingPtr = (TheAnswerToLifeTheUniverseAndEverythingType)GetProcAddress(dllHandle, "TheAnswerToLifeTheUniverseAndEverything");
				if (TheAnswerToLifeTheUniverseAndEverythingPtr)
					retVal = TheAnswerToLifeTheUniverseAndEverythingPtr();
				freeResult = FreeLibrary(dllHandle);
				if (!TheAnswerToLifeTheUniverseAndEverythingPtr || !freeResult)
				{
					cout << "Run Time Error" << endl;
					continue;
				}
				cout << retVal << endl;
			}
			else
				cout << "Failed to load the library - dll file" << endl;
		}
		else if (words.size() == 1 && input[input.length() - 4] == '.'  && input[input.length() - 3] == 'e' && input[input.length() - 2] == 'x' && input[input.length() - 1] == 'e')
		{
			dwRet = GetCurrentDirectory(MAX_PATH, path);
			if (dwRet)
			{
				input = string(path) + "\\" + input;
				strcpy(path, (input.c_str()));
				ZeroMemory(&startupinfo, sizeof(startupinfo));
				startupinfo.cb = sizeof(startupinfo);
				ZeroMemory(&pro2info, sizeof(pro2info));
				if (CreateProcess(TEXT(path), NULL, NULL, NULL, false, CREATE_NEW_CONSOLE, NULL, NULL, &startupinfo, &pro2info))
				{
					WaitForSingleObject(pro2info.hProcess, INFINITE);
					GetExitCodeProcess(pro2info.hProcess, &dwErr);
					CloseHandle(pro2info.hProcess);
				}
				else{
					cout << "unable to execute!" << endl;
				}
			}
		}
	}
	return 0;
}